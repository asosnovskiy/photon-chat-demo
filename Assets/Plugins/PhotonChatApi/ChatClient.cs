﻿// ----------------------------------------------------------------------------------------------------------------------
// <summary>The Photon Chat Api enables clients to connect to a chat server and communicate with other clients.</summary>
// <remarks>ChatClient is the main class of this api.</remarks>
// <copyright company="Exit Games GmbH">Photon Chat Api - Copyright (C) 2014 Exit Games GmbH</copyright>
// ----------------------------------------------------------------------------------------------------------------------

namespace ExitGames.Client.Photon.Chat
{
    using System;
    using System.Diagnostics;
    using System.Collections.Generic;
    using ExitGames.Client.Photon;

    /// <summary>Central class of the Photon Chat API to connect, handle channels and messages.</summary>
    /// <remarks>
    /// This class must be instantiated with a IChatClientListener instance to get the callbacks.
    /// Integrate it into your game loop by calling Service regularly.
    /// Call Connect with an AppId that is setup as Photon Chat application. Note: Connect covers multiple 
    /// messages between this client and the servers. A short workflow will connect you to a chat server.
    /// 
    /// Each ChatClient resembles a user in chat (set in Connect). Each user automatically subscribes a channel 
    /// for incoming private messages and can message any other user privately. 
    /// Before you publish messages in any non-private channel, that channel must be subscribed.
    /// 
    /// PublicChannels is a list of subscribed channels, containing messages and senders.
    /// PrivateChannels contains all incoming and sent private messages.
    /// </remarks>
    public class ChatClient : IPhotonPeerListener
    {
        public string ChatServerAddress = "ns.exitgamescloud.com";
        private readonly Dictionary<ConnectionProtocol, int> ProtocolToNameServerPort = new Dictionary<ConnectionProtocol, int>() { { ConnectionProtocol.Udp, 5058 }, { ConnectionProtocol.Tcp, 4533 }, { ConnectionProtocol.RHttp, 6063 } };
        
        public AuthenticationValues CustomAuthenticationValues { get; set; }
        public ChatDisconnectCause DisconnectedCause { get; private set; }
        public ChatState State { get; private set; }

        public bool CanChat { get { return this.State == ChatState.ConnectedToFrontEnd && this.hasPeer; } }

        /// <summary>The version of your client. A new version also creates a new "virtual app" to separate players from older client versions.</summary>
        public string AppVersion { get; private set; }
        /// <summary>The AppID as assigned from the Photon Cloud. If you host yourself, this is the "regular" Photon Server Application Name (most likely: "LoadBalancing").</summary>
        public string AppId { get; private set; }

        public string UserId { get; private set; }

        public readonly Dictionary<string, ChatChannel> PublicChannels;
        public readonly Dictionary<string, ChatChannel> PrivateChannels;

        private ChatPeer chatPeer = null;
        private readonly IChatClientListener listener = null;

        private string[] subscribeRequests;
        private string[] unsubscribeRequests;

        private bool didAuthenticate;
        private string frontEndAddress;

        private int msDeltaForServiceCalls = 50;
        private int msTimestampOfLastServiceCall;

        private string chatRegion = "EU";

        public string ChatRegion
        {
            get { return chatRegion; }
            set { chatRegion = value; }
        }

        /// <summary>
        /// front end application names
        /// </summary>
        private string AppName { get; set; }

        private bool hasPeer { get { return this.chatPeer != null; }  }

        public ChatClient(IChatClientListener listener)
        {
            this.listener = listener;
            this.State = ChatState.Uninitialized;

            this.PublicChannels = new Dictionary<string, ChatChannel>();
            this.PrivateChannels = new Dictionary<string, ChatChannel>();
        }

        public bool Connect(string appId, string appVersion, string userId, AuthenticationValues authValues)
        {
            return this.Connect(this.ChatServerAddress, ConnectionProtocol.Udp, appId, appVersion, userId, authValues);
        }

        public bool Connect(string address, ConnectionProtocol protocol, string appId, string appVersion, string userId, AuthenticationValues authValues)
        {
            if (!this.hasPeer)
            {
                this.chatPeer = new ChatPeer(this, protocol);
            }
            else
            {
                this.Disconnect();
                if (this.chatPeer.UsedProtocol != protocol)
                {
                    this.chatPeer = new ChatPeer(this, protocol);
                }
            }
            
            this.DisconnectedCause = ChatDisconnectCause.None;

            this.CustomAuthenticationValues = authValues;
            this.UserId = userId;
            this.AppId = appId;
            this.AppVersion = appVersion;
            this.AppName = "chat";
            this.didAuthenticate = false;
            this.msDeltaForServiceCalls = 100;


            // clean all channels
            this.PublicChannels.Clear();
            this.PrivateChannels.Clear();
            this.subscribeRequests = null;
            this.unsubscribeRequests = null;
            
            if (!address.Contains(":"))
            {
                int port = 0;
                ProtocolToNameServerPort.TryGetValue(protocol, out port);
                address = string.Format("{0}:{1}", address, port);
            }

            bool isConnecting = this.chatPeer.Connect(address, "NameServer");
            if (isConnecting)
            {
                this.State = ChatState.ConnectingToNameServer;
            }
            return isConnecting;
        }

        /// <summary>
        /// Must be called regularly to keep connection between client and server alive and to process incoming messages.
        /// </summary>
        /// <remarks>
        /// This method limits the effort it does automatically using the private variable msDeltaForServiceCalls. 
        /// That value is lower for connect and multiplied by 4 when chat-server connection is ready.
        /// </remarks>
        public void Service()
        {
            if (this.hasPeer && (Environment.TickCount - msTimestampOfLastServiceCall > msDeltaForServiceCalls || msTimestampOfLastServiceCall == 0))
            {
                msTimestampOfLastServiceCall = Environment.TickCount;
                this.chatPeer.Service();  //TODO: make sure to call service regularly. in best case it could be integrated into PhotonHandler.FallbackSendAckThread()!
            }
        }

        public void Disconnect()
        {
            if (this.hasPeer && this.chatPeer.PeerState != PeerStateValue.Disconnected)
            {
                this.chatPeer.Disconnect();
            }
        }

        /// <summary>Sends operation to subscribe to a list of channels by name.</summary>
        /// <param name="channels">List of channels to subscribe to.</param>
        /// <returns>If the operation could be sent at all (Example: Fails if not connected to Chat Server).</returns>
        public bool Subscribe(string[] channels)
        {
            return this.Subscribe(channels, 0);
        }

        /// <summary>
        /// Sends operation to subscribe client to channels, optionally fetching messages newer than a specific ID.
        /// </summary>
        /// <param name="channels">List of channels to subscribe to.</param>
        /// <param name="messagesFromHistory">0: no history. 1 and higher: number of messages in history. -1: all history.</param>
        /// <returns>If the operation could be sent at all (Example: Fails if not connected to Chat Server).</returns>
        public bool Subscribe(string[] channels, int messagesFromHistory)
        {
            if (!this.CanChat)
            {
                // TODO: log error
                return false;
            }

            if (this.subscribeRequests != null)
            {
                // TODO: log error
                return false;
            }

            bool sent = this.SendChannelOperation(channels, (byte)ChatOperationCode.Subscribe, messagesFromHistory);
            if (sent)
            {
                this.subscribeRequests = channels;
            }

            return false;
        }

        /// <summary>Unsubscribes from a list of channels, which stops getting messages from those.</summary>
        /// <remarks>
        /// The client will remove these channels from the PublicChannels dictionary once the server sent a response to this request.
        /// Only one Unsubscribe request can be handled at any time. 
        /// Wait for the callback of OnUnsubscribed before you unsubscribe more channels.
        /// Unsubscribe will return false and not send further requests if one request is still without response.
        /// </remarks>
        /// <param name="channels">Names of channels to unsubscribe.</param>
        /// <returns>False, if some Unsubscribe operation is not yet finished.</returns>
        public bool Unsubscribe(string[] channels)
        {
            if (!this.CanChat)
            {
                // TODO: log error
                return false;
            }

            if (this.unsubscribeRequests != null)
            {
                // TODO: log error
                return false;
            }

            bool sent = SendChannelOperation(channels, (byte)ChatOperationCode.Unsubscribe, 0);
            if (sent)
            {
                this.unsubscribeRequests = channels;
            }

            return sent;
        }

        /// <summary>Sends a message to a public channel which this client subscribed to.</summary>
        /// <remarks>
        /// Before you publish to a channel, you have to subscribe it. 
        /// Everyone in that channel will get the message.
        /// </remarks>
        /// <param name="channelName">Name of the channel to publish to.</param>
        /// <param name="message">Your message (string or any serializable data).</param>
        /// <returns>False if the client is not yet ready to send messages.</returns>
        public bool PublishMessage(string channelName, object message)
        {
            if (!this.CanChat)
            {
                // TODO: log error
                return false;
            }

            Dictionary<byte, object> parameters = new Dictionary<byte, object>
                {
                    { (byte)ChatParameterCode.Channel, channelName },
                    { (byte)ChatParameterCode.Message, message }
                };

            return this.chatPeer.OpCustom((byte)ChatOperationCode.Publish, parameters, true);
        }

        /// <summary>
        /// Sends a private message to a single target user. Calls OnPrivateMessage on the receiving client.
        /// </summary>
        /// <param name="target">Username to send this message to.</param>
        /// <param name="message">The message you want to send. Can be a simple string or anything serializable.</param>
        /// <returns>True if this clients can send the message to the server.</returns>
        public bool SendPrivateMessage(string target, object message)
        {
            if (!this.CanChat)
            {
                // TODO: log error
                return false;
            }

            Dictionary<byte, object> parameters = new Dictionary<byte, object>
                {
                    { ChatParameterCode.UserId, target },
                    { ChatParameterCode.Message, message }
                };

            bool sent = this.chatPeer.OpCustom((byte)ChatOperationCode.SendPrivate, parameters, true);
            return sent;
        }

        /// <summary>Sets the user's status (pre-defined or custom) and an optional message.</summary>
        /// <remarks>
        /// The predefined status values can be found in class ChatUserStatus.
        /// State ChatUserStatus.Invisible will make you offline for everyone and send no message.
        /// </remarks>
        /// <param name="status">Predefined states are in class ChatUserStatus. Other values can be used at will.</param>
        /// <param name="message">Optional string message or null.</param>
        /// <param name="skipMessage">If true, the message gets ignored. It can be null but won't replace any current message.</param>
        /// <returns>True if the operation gets called on the server.</returns>
        private bool SetOnlineStatus(int status, object message, bool skipMessage)
        {
            if (!this.CanChat)
            {
                // TODO: log error
                return false;
            }

            Dictionary<byte, object> parameters = new Dictionary<byte, object>
                {
                    { ChatParameterCode.Status, status }, 
                };

            if (skipMessage)
            {
                parameters[ChatParameterCode.SkipMessage] = true;
            }
            else
            {
                parameters[ChatParameterCode.Message] = message;
            }
            return this.chatPeer.OpCustom(ChatOperationCode.UpdateStatus, parameters, true);
        }

        /// <summary>Sets the user's status without changing your status-message.</summary>
        /// <remarks>
        /// The predefined status values can be found in class ChatUserStatus.
        /// State ChatUserStatus.Invisible will make you offline for everyone and send no message.
        /// </remarks>
        /// <param name="status">Predefined states are in class ChatUserStatus. Other values can be used at will.</param>
        /// <returns>True if the operation gets called on the server.</returns>
        public bool SetOnlineStatus(int status)
        {
            return SetOnlineStatus(status, null, true);
        }
        /// <summary>Sets the user's status without changing your status-message.</summary>
        /// <remarks>
        /// The predefined status values can be found in class ChatUserStatus.
        /// State ChatUserStatus.Invisible will make you offline for everyone and send no message.
        /// </remarks>
        /// <param name="status">Predefined states are in class ChatUserStatus. Other values can be used at will.</param>
        /// <param name="message">Also sets a status-message which your friends can get.</param>
        /// <returns>True if the operation gets called on the server.</returns>
        public bool SetOnlineStatus(int status, object message)
        {
            return SetOnlineStatus(status, message, false);
        }

        /// <summary>
        /// Sets your friend list on the Chat Server which will send you status updates for those.
        /// </summary>
        /// <param name="friends">Array of friend userIds.</param>
        /// <returns>If the operation could be sent.</returns>
        public bool SetFriendList(string[] friends)
        {
            if (!this.CanChat)
            {
                // TODO: log error
                return false;
            }

            Dictionary<byte, object> parameters = new Dictionary<byte, object>
                {
                    { ChatParameterCode.Friends, friends }, 
                };
            return this.chatPeer.OpCustom(ChatOperationCode.FriendList, parameters, true);
        }

        /// <summary>
        /// Get you the (locally used) channel name for the chat between this client and another user.
        /// </summary>
        /// <param name="userName">Remote user's name or UserId.</param>
        /// <returns>The (locally used) channel name for a private channel.</returns>
        public string GetPrivateChannelNameByUser(string userName)
        {
            return string.Format("{0}:{1}", this.UserId, userName);
        }

        /// <summary>
        /// Simplified access to either private or public channels by name.
        /// </summary>
        /// <param name="channelName">Name of the channel to get. For private channels, the channel-name is composed of both user's names.</param>
        /// <param name="isPrivate">Define if you expect a private or public channel.</param>
        /// <param name="channel">Out parameter gives you the found channel, if any.</param>
        /// <returns>True if the channel was found.</returns>
        public bool TryGetChannel(string channelName, bool isPrivate, out ChatChannel channel)
        {
            if (!isPrivate)
            {
               return this.PublicChannels.TryGetValue(channelName, out channel);
            }
            else
            {
                return this.PrivateChannels.TryGetValue(channelName, out channel);
            }
        }

        public void SendAcksOnly()
        {
            if (this.chatPeer != null) this.chatPeer.SendAcksOnly();
        }


        #region Private methods area

        #region IPhotonPeerListener implementation

        void IPhotonPeerListener.DebugReturn(DebugLevel level, string message)
        {
            Debug.WriteLine(message);
        }

        void IPhotonPeerListener.OnEvent(EventData eventData)
        {
            switch (eventData.Code)
            {
                case ChatEventCode.ChatMessages:
                    this.HandleChatMessagesEvent(eventData);
                    break;
                case ChatEventCode.PrivateMessage:
                    this.HandlePrivateMessageEvent(eventData);
                    break;
                case ChatEventCode.StatusUpdate:
                    this.HandleStatusUpdate(eventData);
                    break;
            }
        }

        void IPhotonPeerListener.OnOperationResponse(OperationResponse operationResponse)
        {
            switch (operationResponse.OperationCode)
            {
                case (byte)ChatOperationCode.Authenticate:
                    this.HandleAuthResponse(operationResponse);
                    break;
                case (byte)ChatOperationCode.Subscribe:
                    this.HandleSubscribeResponse(operationResponse);
                    break;
                case (byte)ChatOperationCode.Publish:
                    break;
                case (byte)ChatOperationCode.SendPrivate:
                    break;
                case (byte)ChatOperationCode.Unsubscribe:
                    this.HandleUnsubscribeResponse(operationResponse);
                    break;
                default:
                    break;
            }
        }

        void IPhotonPeerListener.OnStatusChanged(StatusCode statusCode)
        {
            switch (statusCode)
            {
                case StatusCode.Connect:
                    this.chatPeer.EstablishEncryption();
                    if (this.State == ChatState.ConnectingToNameServer)
                    {
                        this.State = ChatState.ConnectedToNameServer;
                        this.listener.OnChatStateChange(this.State);
                    }
                    else if (this.State == ChatState.ConnectingToFrontEnd)
                    {
                        this.AuthenticateOnFrontEnd();
                    }
                    break;
                case StatusCode.EncryptionEstablished:
                    // once encryption is availble, the client should send one (secure) authenticate. it includes the AppId (which identifies your app on the Photon Cloud)
                    if (!this.didAuthenticate)
                    {
                        this.didAuthenticate = this.chatPeer.AuthenticateOnNameServer(this.AppId, this.AppVersion, this.chatRegion, this.UserId, this.CustomAuthenticationValues);
                        if (!this.didAuthenticate)
                        {
                            ((IPhotonPeerListener) this).DebugReturn(DebugLevel.ERROR, "Error calling OpAuthenticate! Did not work. Check log output, CustomAuthenticationValues and if you're connected. State: " + this.State);
                        }
                    }
                    break;
                case StatusCode.EncryptionFailedToEstablish:
                    this.State = ChatState.Disconnecting;
                    this.chatPeer.Disconnect();
                    break;
                case StatusCode.Disconnect:
                    if (this.State == ChatState.Authenticated)
                    {
                        this.ConnectToFrontEnd();
                    }
                    else
                    {
                        this.State = ChatState.Disconnected;
                        this.listener.OnChatStateChange(ChatState.Disconnected);
                        this.listener.OnDisconnected();
                    }
                    break;
            }
        }

#if SDK_V4
        void IPhotonPeerListener.OnMessage(object msg)
        {
            // in v4 interface IPhotonPeerListener
            return;
        }
#endif

        #endregion

        private bool SendChannelOperation(string[] channels, byte operation, int historyLength)
        {
            Dictionary<byte, object> opParameters = new Dictionary<byte, object> { { (byte)ChatParameterCode.Channels, channels } };

            if (historyLength != 0)
            {
                opParameters.Add((byte)ChatParameterCode.HistoryLength, historyLength);
            }

            return this.chatPeer.OpCustom(operation, opParameters, true);
        }

        private void HandlePrivateMessageEvent(EventData eventData)
        {
            var message = (object)eventData.Parameters[(byte)ChatParameterCode.Message];
            var sender = (string)eventData.Parameters[(byte)ChatParameterCode.Sender];

            string channelName;
            if (this.UserId != null && this.UserId.Equals(sender))
            {
                var target = (string)eventData.Parameters[(byte)ChatParameterCode.UserId];
                channelName = this.GetPrivateChannelNameByUser(target);
            }
            else
            {
                channelName = this.GetPrivateChannelNameByUser(sender);
            }

            ChatChannel channel;
            if (!this.PrivateChannels.TryGetValue(channelName, out channel))
            {
                channel = new ChatChannel(channelName);
                channel.IsPrivate = true;
                this.PrivateChannels.Add(channel.Name, channel);
            }

            channel.Add(sender, message);
            this.listener.OnPrivateMessage(sender, message, channelName);
        }

        private void HandleChatMessagesEvent(EventData eventData)
        {
            var messages = (object[])eventData.Parameters[(byte)ChatParameterCode.Messages];
            var senders = (string[])eventData.Parameters[(byte)ChatParameterCode.Senders];
            var channelName = (string)eventData.Parameters[(byte)ChatParameterCode.Channel];
            var lastId = (int)eventData.Parameters[(byte)ChatParameterCode.MsgId];

            ChatChannel channel;
            if (!this.PublicChannels.TryGetValue(channelName, out channel))
            {
                // TODO: log that channel wasn't found
                return;
            }

            channel.Add(senders, messages, lastId);
            this.listener.OnGetMessages(channelName, senders, messages);
        }

        private void HandleSubscribeResponse(OperationResponse operationResponse)
        {
            if (this.subscribeRequests == null)
            {
                System.Diagnostics.Debug.Assert(false);
                return;
            }
            if (operationResponse.ReturnCode != 0)
            {
                // TODO: log something
                return;
            }

            bool[] results = (bool[])operationResponse.Parameters[ChatParameterCode.SubscribeResults];
            string[] channelsInRequest = this.subscribeRequests;
            for (int i = 0; i < channelsInRequest.Length; i++)
            {
                if (results[i])
                {
                    string channelName = channelsInRequest[i];
                    if (!this.PublicChannels.ContainsKey(channelName))
                    {
                        ChatChannel channel = new ChatChannel(channelName);
                        this.PublicChannels.Add(channel.Name, channel);
                    }
                }
            }

            this.subscribeRequests = null;
            this.listener.OnSubscribed(channelsInRequest, results);
        }

        private void HandleUnsubscribeResponse(OperationResponse operationResponse)
        {
            if (this.unsubscribeRequests == null)
            {
                System.Diagnostics.Debug.Assert(false);
                return;
            }
            if (operationResponse.ReturnCode != 0)
            {
                // TODO: log something
                return;
            }

            string[] channelsInRequest = this.unsubscribeRequests;
            for (int i = 0; i < channelsInRequest.Length; i++)
            {
                string channelName = channelsInRequest[i];
                this.PublicChannels.Remove(channelName);
            }

            this.unsubscribeRequests = null;
            this.listener.OnUnsubscribed(channelsInRequest);
        }

        private void HandleAuthResponse(OperationResponse operationResponse)
        {
            ((IPhotonPeerListener)this).DebugReturn(DebugLevel.ERROR, operationResponse.ToStringFull() + " on: " + this.ChatServerAddress);
            if (operationResponse.ReturnCode == 0)
            {
                if (this.State == ChatState.ConnectedToNameServer)
                {
                    this.State = ChatState.Authenticated;
                    this.listener.OnChatStateChange(this.State);

                    if (operationResponse.Parameters.ContainsKey(ParameterCode.Secret))
                    {
                        if (this.CustomAuthenticationValues == null)
                        {
                            this.CustomAuthenticationValues = new AuthenticationValues();
                        }
                        this.CustomAuthenticationValues.Secret = operationResponse[ParameterCode.Secret] as string;
                        this.frontEndAddress = (string) operationResponse[ParameterCode.Address];

                        // we disconnect and status handler starts to connect to front end
                        this.chatPeer.Disconnect();
                    }
                    else
                    {
                        //TODO: error reaction!
                    }
                }
                else if (this.State == ChatState.ConnectingToFrontEnd)
                {
                    this.msDeltaForServiceCalls = this.msDeltaForServiceCalls * 4;  // when we arrived on chat server: limit Service calls some more

                    this.State = ChatState.ConnectedToFrontEnd;
                    this.listener.OnChatStateChange(this.State);
                    this.listener.OnConnected();
                }
            }
            else
            {
                ((IPhotonPeerListener)this).DebugReturn(DebugLevel.ERROR, operationResponse.ToStringFull() + " NS: " + this.ChatServerAddress + " FrontEnd: " + this.frontEndAddress);

                switch (operationResponse.ReturnCode)
                {
                    case ErrorCode.InvalidAuthentication:
                        this.DisconnectedCause = ChatDisconnectCause.InvalidAuthentication;
                        break;
                    case ErrorCode.CustomAuthenticationFailed:
                        this.DisconnectedCause = ChatDisconnectCause.CustomAuthenticationFailed;
                        break;
                    case ErrorCode.InvalidRegion:
                        this.DisconnectedCause = ChatDisconnectCause.InvalidRegion;
                        break;
                    case ErrorCode.MaxCcuReached:
                        this.DisconnectedCause = ChatDisconnectCause.MaxCcuReached;
                        break;
                    case ErrorCode.OperationNotAllowedInCurrentState:
                        this.DisconnectedCause = ChatDisconnectCause.OperationNotAllowedInCurrentState;
                        break;
                }

                this.State = ChatState.Disconnecting;
                this.chatPeer.Disconnect();
            }
        }

        private void HandleStatusUpdate(EventData eventData)
        {
            var user = (string)eventData.Parameters[ChatParameterCode.Sender];
            var status = (int)eventData.Parameters[ChatParameterCode.Status];

            object message = null;
            bool gotMessage = eventData.Parameters.ContainsKey(ChatParameterCode.Message);
            if (gotMessage)
            {
                message = eventData.Parameters[ChatParameterCode.Message];
            }

            this.listener.OnStatusUpdate(user, status, gotMessage, message);
        }

        private void ConnectToFrontEnd()
        {
            this.State = ChatState.ConnectingToFrontEnd;

            this.chatPeer.Connect(frontEndAddress, AppName);
        }

        private bool AuthenticateOnFrontEnd()
        {
            if (CustomAuthenticationValues != null)
            {
                var d = new Dictionary<byte, object> {{(byte)ChatParameterCode.Secret, CustomAuthenticationValues.Secret}};
                return this.chatPeer.OpCustom((byte)ChatOperationCode.Authenticate, d, true);
            }
            else
            {
                Debug.WriteLine("Can't authenticate on front end server. CustomAuthValues is null");
            }
            return false;
        }

        #endregion
    }
}
